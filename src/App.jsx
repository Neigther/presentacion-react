import './App.css'
import { Perfilmax } from './Perfil/Perfil1/Perfilmax'
import { Info } from './Perfil/Informacion/Info'
import { Redessociales } from './Perfil/Redes/Redesociales'

function App() {

  return (
    <div>
      
      {/* <h1>Hola Mundo</h1>
      <Componente1/>
      <Componente2/> */}
      <Perfilmax/>
      <Info/>
      <Redessociales/>
    </div>
  )
}

export default App
