import './Styles2.css'
import React from 'react'
import Dios from '/src/assets/Dios.jpg'

export const Info = () => {
    return (
        <>
            <div className="todo">
                <div className="info">
                    <p>Nombre:<i> Morales Huertos Ismael</i></p>
                    <p>Carrera:<i> ISC</i></p>
                    <p>Semestre:<i> 8° Semestre</i></p>
                    <p>Signo:<i> Acuario</i></p>
                    <p>Raza:<i> Dios</i></p>
                </div>
                <div className="foto">
                    <img src={Dios} />
                </div>
                <div className="info2">
                    <ul>
                        <p>Color favorito:<i> Ninguno</i></p>
                        <p>Deporte:<i> Ninguno</i></p>
                        <p>Estado Civil:<i> Soltero</i></p>
                        <p>Comida Favorita:<i> Sus mentiras</i></p>
                        <p>Sueños:<i> No se</i></p>
                    </ul>
                </div>
            </div>
        </>
    )
}