import React from 'react'
import './Styles3.css'
import facebook from '/src/assets/facebook.png'
import telegram from '/src/assets/telegram.png'
import whatsapp from '/src/assets/whatsapp.png'
import instagram from '/src/assets/instagram.png'

export const Redessociales = () => {
    return (
        <>
            <div className="Redes-Sociales">
                <div className="parce">
                    <a href="https://www.facebook.com/Neutroegaymer/"><img src={facebook} /></a>
                </div>
                <div className="parce">
                    <a href="https://www.instagram.com/0101001010a14p2/"><img src={telegram} /></a>
                </div>
                <div className="parce">
                    <a href="https://web.whatsapp.com/"><img src={whatsapp} /></a>
                </div>
                <div className="parce">
                    <a href="https://web.telegram.org/z/"><img src={instagram} /></a>
                </div>
            </div>
        </>
    )
}